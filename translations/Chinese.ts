<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../screen/workspace.cpp" line="658"/>
        <source>save file</source>
        <translation type="unfinished">保存图片</translation>
    </message>
</context>
<context>
    <name>TextBar</name>
    <message>
        <location filename="../properties/textbar.cpp" line="77"/>
        <source>bold</source>
        <translation type="unfinished">粗体</translation>
    </message>
    <message>
        <location filename="../properties/textbar.cpp" line="82"/>
        <source>italic</source>
        <translation type="unfinished">斜体</translation>
    </message>
    <message>
        <location filename="../properties/textbar.cpp" line="87"/>
        <source>underline</source>
        <translation type="unfinished">下划线</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../screen/toolbar.cpp" line="79"/>
        <source>rectangle</source>
        <translation type="unfinished">矩形</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="80"/>
        <source>ellipse</source>
        <translation type="unfinished">椭圆</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="81"/>
        <source>line</source>
        <translation type="unfinished">直线</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="82"/>
        <source>arrow</source>
        <translation type="unfinished">箭头</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="83"/>
        <source>pie</source>
        <translation type="unfinished">铅笔</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="84"/>
        <source>text</source>
        <translation type="unfinished">文本</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="85"/>
        <source>mosaic</source>
        <translation type="unfinished">马赛克</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="96"/>
        <source>undo</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="97"/>
        <source>redo</source>
        <translation type="unfinished">重做</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="104"/>
        <source>download</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="105"/>
        <source>exit</source>
        <translation type="unfinished">退出</translation>
    </message>
    <message>
        <location filename="../screen/toolbar.cpp" line="106"/>
        <source>finish</source>
        <translation type="unfinished">完成</translation>
    </message>
</context>
</TS>
